import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {ViewService} from '../home/view.service';
import {ActivatedRoute,Router} from '@angular/router';
@Component({
  selector: 'app-designer',
  templateUrl: './designer.component.html',
  styleUrls: ['./designer.component.scss']
})
export class DesignerComponent implements OnInit {

@Output()display= new EventEmitter();

  nombre_imagen:String='';
  designer:String='';

  // si la imagen se mando con exito 
  image_state:boolean=false;
  id_imagen=0;

  url_id:String='';

  constructor(private service:ViewService,private url_router:ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.url_id= this.url_router.snapshot.paramMap.get('id');
  }

  CreateImage(){
    let data:any={
      "nombre_imagen":this.nombre_imagen,
      "designer":this.designer
    }
    let state_UserImg:boolean=false;
    this.service.SetImage(data).subscribe((data:any)=>{
      console.log("datos que trae el servicio de SetImage",data);
      if(data.result.protocol41==true){
        this.image_state=true;
        this.id_imagen=data.result.insertId;
      }
      if (this.image_state==true){
        let send_data:any={
          "id_usuario":this.url_id,
          "id_imagen":this.id_imagen
        }
        this.service.ImageUser(send_data).subscribe((data:any)=>{
          console.log("datos que trae el servicio de ImageUser",data);
          if (data.state==true){
            console.log("entro en la condcion de la navegacion");
            this.router.navigate(['home/'+this.url_id]);
          }
        });

      }
    

    });

    
  }

  

  
}
