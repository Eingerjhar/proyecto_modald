import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent} from './login/login/login.component'
import { CreateUserComponent} from './login/create-user/create-user.component'
import { HomeComponent} from './home/home/home.component';

const routes: Routes = [
  {path: '', redirectTo :'/login', pathMatch:'full'},
  {path:'login',component:LoginComponent},
  {path:'login/create',component:CreateUserComponent},
  {path:'home/:id',component:HomeComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
