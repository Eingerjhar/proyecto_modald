import { Component, OnInit, OnChanges } from '@angular/core';
import { LoginServiceService } from '../login-service.service';
import { Router} from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnChanges {

  // datos de los usuarios para la validacion de usuarios
  data_user: any;
  data_copy: any;
  // variables para hacer la peticion de login
  nombre_usuario = '';
  password = '';

  // mesnsaje receptor de errores
  message:String='';


  constructor(private service: LoginServiceService, private router : Router) { }


  ngAfterViewInit(): void {

  }
  ngOnInit() {
    this.service.getUsers().subscribe(data => {
      this.data_user = data;
      console.log("datos que trae le metodo get usuarios", this.data_user);
      this.data_copy = data;
    });

    console.log("datos que muestra despyes del metodo ", this.data_user);
  }



  ngOnChanges(changes): void {
    console.log("cambios que se han realizado ", changes);
  }

  mychange(e) {
    console.log(e.value); // updated value
  }

  Conectarse(){
    let data:any={
      "nombre_usuario":this.nombre_usuario,
      "password": this.password
    }
    console.log("datos que estan en prueba",this.nombre_usuario,this.password);
    this.service.login(data).subscribe((data:any)=>{
      if(data[0]==undefined){
        console.log("el usuario no se encuentra registrado");
      }else{
       this.router.navigate(['/home/'+data[0].id]);
      }
    });
  }

  CreateUser(){
    this.router.navigate(['login/create']);
  }
}


