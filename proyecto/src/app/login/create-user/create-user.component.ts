import { Component, OnInit } from '@angular/core';
import {LoginServiceService} from '../login-service.service'
import { Router} from '@angular/router';
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

   password:String='';
   data:any={
    "nombre_usuario":'',
    "contraseña":'',
    "nombre":'',
    "edad":'',
    "imagen":''
  }
   

  constructor(private service:LoginServiceService, private router:Router) { }

  ngOnInit() {
  }
  
  Create(){
  this.data.contraseña=this.password;
    this.service.CreateUser(this.data).subscribe((data:any)=>{
      console.log("mensaje que muestra el servicio de crear usuario ",data);
      if(data.state==false){
        console.log("no se pudo crear el usuario ")
      }else{
        this.router.navigate(['/login']);
      }
    });
  }

}
