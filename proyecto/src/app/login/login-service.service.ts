import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const api='http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(private http : HttpClient) { }

  getUsers() : Observable<object>{
    return this.http.get(api).pipe(
      tap(_ => console.log("envio de solicitud getUsers")),
      map(res => {
        return res;
      }));
  }

  // :Observable<object>
  
  login(send_data) :Observable<object>{
    return this.http.post(`${api}/login`,send_data).pipe(
      map((res:any)=>{
        console.log("mensaje que retorna el servicio ", res);
        return res;
      })
    )
  }

  CreateUser(send_data):Observable<object>{
    return this.http.post(`${api}/login/createUser`,send_data).pipe(
      map(data=>{
        console.log("datos que envia el servicio de crear usuario",data);
        return data;
      })
    )
  }
}
