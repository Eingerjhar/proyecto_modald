import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HomeComponent} from './home/home/home.component'
import { CreateUserComponent } from './login/create-user/create-user.component';
import {LoginServiceService} from './login/login-service.service'
import { HttpClientModule } from '@angular/common/http'
import {LoginModule} from './login/login/login.module';
import { ViewProfileComponent } from './home/view-profile/view-profile.component'
import { EditProfileComponent } from './home/edit-profile/edit-profile.component';
import { DesignerComponent } from './designer/designer.component';
import {ViewService} from './home/view.service'

@NgModule({
  declarations: [
    AppComponent,
    CreateUserComponent,
    ViewProfileComponent,
    HomeComponent,
    EditProfileComponent,
    DesignerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    LoginModule
  ],
  providers: [
    LoginServiceService,
    ViewService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
