import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewProfileComponent} from './view-profile/view-profile.component'
import {FormsModule} from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component'
@NgModule({
  declarations: [ViewProfileComponent, HomeComponent, EditProfileComponent],
  imports: [
    FormsModule,
    CommonModule
  ],
  exports:[
    ViewProfileComponent,
    HomeComponent,
    EditProfileComponent
  ]
})
export class HomeModule { }
