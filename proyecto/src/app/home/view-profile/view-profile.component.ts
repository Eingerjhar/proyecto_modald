import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import {user} from '../../interfaces/user.component'
@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {
  @Input()user:any;
  @Input()user_image:any;
  
  // variable con un nuevo tipo de dato definido en interfaces
  data_user:user;
  imagen:any;

  // vairbale para el cambio de vista de componentes
  change_display=true;
  constructor() { }

  ngOnInit() {
   
  }
  ngOnChanges(changes): void {
    console.log("cambios",changes);
    if (changes.hasOwnProperty('user') && this.user != undefined){
      this.data_user=this.user.result[0];

      console.log("datos que se encuentran en data_user",this.data_user);
      
    } 

    if(changes.hasOwnProperty('user_image') && this.user_image != undefined){
      this.imagen=this.user_image.result;
      console.log("esto es lo que trae user",this.user_image)
    }
      
    
    }

    // para cuando se selecciona una imagen poder obtener la id 
    prueba(e){
      console.log("datos que tiene el metodo de prueba",e);
    }
    
    editProfile(e){
     if(typeof(e)=="boolean"){
      this.change_display=e;
     }else{
       this.change_display=e.display;
     }
    }
  }
  


