import { Component, OnInit, SimpleChanges } from '@angular/core';
import {ViewService} from '../view.service'
import {ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data_user:any;
  getImage:any;
  url_id:String='';

  // cambio de vista entre view-profile y designer
  display_home:boolean=true;
  display_designer:boolean=false;
  constructor(private service: ViewService, private url_router:ActivatedRoute, private router:Router) {
    this.url_id= this.url_router.snapshot.paramMap.get('id');
    console.log("id que recoge el url_router",this.url_id);
   }

  ngOnInit() {
    this.service.GetUser(this.url_id).subscribe((data:any)=>{
      console.log("comienzo",data);
     
      if(data==undefined || data==null){
        console.log("ha ocurrido un error al traer al usuario");

      }else{
        console.log("datos traidos: ",data);
        this.data_user=data;
      }
    });

    this.service.GetUserImage(this.url_id).subscribe((data:any)=>{
      console.log("respondio el servicio de GetUserImage");
      
    if(data==undefined || data==null){
      console.log("ha ocurrido un error al traer las imagenes del usuario");

    }else{
      console.log("datos traidos del servicio GetUserImage : ",data);
      this.getImage=data;
    }
  });
  }
  ngOnChanges(changes: SimpleChanges): void {
    
  }

  redirect(e){
    if(e=='designer'){
     this.display_designer=true;
     this.display_home=false;
    }
    if(e=='home'){
      this.display_designer=false;
      this.display_home=true;
    }
  }
  
}

