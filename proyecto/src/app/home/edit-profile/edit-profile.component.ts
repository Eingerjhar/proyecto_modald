import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import {user} from '../../interfaces/user.component';
import {ViewService} from '../view.service'
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  // cuando actualize el perfil pueda cambiar la vista denuevo a view_profile
  @Output() display = new EventEmitter();
  @Input() data_user:any;
  
  
  //variable para la actualizacion de los datos 
  update:user;

  password:String='';
  constructor(private service:ViewService) { }

  ngOnInit() {
   this.password=this.data_user.contraseña;

  }

  ngOnChanges(changes): void {
    console.log("cambios",changes);
    if (changes.hasOwnProperty('data_user') && this.data_user != undefined){
      // this.update=this.data_user.result[0];

      console.log("datos que se encuentran en data_user",this.data_user);
      
    } 
  }
  Update(){
    this.data_user.contraseña=this.password;
    this.service.UpdateUser(this.data_user).subscribe((data:any)=>{
      console.log("datos que trae el servicio de UpdateUser",data);

    });
    this.display.emit({
      display:true
    });
  }
}
