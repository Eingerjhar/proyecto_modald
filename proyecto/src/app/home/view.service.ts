import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const api = 'http://localhost:3000';
@Injectable({
  providedIn: 'root'
})

export class ViewService {

  constructor(private http: HttpClient) { }

  GetUser(id): Observable<any> {
    return this.http.get(`${api}/home/${id}`).pipe(
      map(res => {
        console.log("datos que envia el servicio de GetUser", res);
        return res;
      })
    );
  }

  GetUserImage(id): Observable<any> {
    return this.http.get(`${api}/get/userImage/${id}`).pipe(
      map(res => {
        console.log("datos que envia el servicio GetUserImage");
        return res;
      })
    );
  }

  UpdateUser(send_data): Observable<any> {
    return this.http.put(`${api}/${send_data.id}`,send_data).pipe(
      map(res => {
        console.log("datos que envia el servicio GetUserImage");
        return res;
      })
    );
  }

  SetImage(send_data): Observable<any> {
    return this.http.post(`${api}/createImagen`,send_data).pipe(
      map(res => {
        console.log("datos que envia el servicio SetImage");
        return res;
      })
    );
  }
  
  ImageUser(send_data): Observable<any> {
    return this.http.post(`${api}/create/userImage`,send_data).pipe(
      map(res => {
        console.log("datos que envia el servicio SetImage");
        return res;
      })
    );
  }


}
