create database ProyectoIS;

use ProyectoIS;

/* para la imagen nada mas puede sorportar imagenes de hasta 16 mb */
create table Imagen(
	id int(4) not null auto_increment primary key,
	imagen mediumblob,
	nombre_imagen varchar(200) 
);

/*tabla que contiene e login y datos de usuario*/
create table usuario(
	id int(4) not null auto_increment primary key,
    nombre_usuario varchar(200) not null,
    contraseña varchar(200) not null,
    nombre varchar(65),
    edad int(3)
);

create table imagen_usuario(
	id_usuario int (4) not null,
    id_imagen int(4) not null ,
    tiempo timestamp default current_timestamp primary key
);

 

/* creacion de las constrains */
alter table imagen_usuario add constraint FK_usario foreign key (id_usuario) references usuario(id);
alter table imagen_usuario add constraint FK_Imagen foreign key (id_imagen) references Imagen(id);

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '40927745729';

alter table createusers add column imagen varchar(400);

select * from createusers;
select * from imagen;
select * from imagen_usuario;

select * from createusers where  nombre_usuario='pppp' and contraseña='12345' LIMIT 0, 1000;

alter table imagen change column  imagen designer varchar(500);

select * from imagen_usuario g
inner join imagen i on i.id=g.id_imagen
inner join createusers u on g.id_usuario and u.id =10;



