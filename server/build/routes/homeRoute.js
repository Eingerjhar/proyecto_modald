"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const homeController_1 = require("../controllers/homeController");
class homeRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        // this.router.get('/api/home', (req,res) =>{
        //     res.send("entro en el home");
        // });
        // prueba con la base de datos
        this.router.get('/home/:id', homeController_1.home.home);
        // this.router.post()
    }
}
const Home = new homeRoute();
exports.default = Home.router;
