"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const imagenController_1 = require("../controllers/imagenController");
class loginRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        // this.router.get('/', (req,res) =>{
        //     res.send("entro en el loginRoutes");
        // });
        // prueba con base de datos
        // pensar si se va a utilizar la lista de todos los uusarios para la prueba
        this.router.get('/listImage', imagenController_1.imagen.ListImage);
        // post para que me valide los datos que recogo al hacer login
        this.router.get('/edit', imagenController_1.imagen.SelectImage);
        // estos dos post son para la subida de la imagen en sus respectivas tablas
        // estos dos post deben de ser usados al mismo tiempo(mirar la base de datos para entender)
        this.router.post('/createImagen', imagenController_1.imagen.SetImage);
        this.router.post('/create/userImage', imagenController_1.imagen.UserImage);
        this.router.get('/get/userImage/:id', imagenController_1.imagen.GetUserImage);
    }
}
const image = new loginRoute();
exports.default = image.router;
