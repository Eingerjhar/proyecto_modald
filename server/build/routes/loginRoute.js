"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const loginController_1 = require("../controllers/loginController");
class loginRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        // this.router.get('/', (req,res) =>{
        //     res.send("entro en el loginRoutes");
        // });
        // prueba con base de datos
        // pensar si se va a utilizar la lista de todos los uusarios para la prueba
        this.router.get('/', loginController_1.login.getusers);
        // post para que me valide los datos que recogo al hacer login
        this.router.post('/login', loginController_1.login.login);
        this.router.post('/login/createUser', loginController_1.login.createUser);
        this.router.delete('/:id', loginController_1.login.deleteUser);
        // pensar en que componente va a llamar este metodo
        this.router.put('/:id', loginController_1.login.updateUSer);
    }
}
const Login = new loginRoute();
exports.default = Login.router;
