"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const loginRoute_1 = __importDefault(require("./routes/loginRoute"));
const homeRoute_1 = __importDefault(require("./routes/homeRoute"));
const imagenRoute_1 = __importDefault(require("./routes/imagenRoute"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    // configuracion del servidor 
    config() {
        // process.env.PORT es por si necesito el puerto de a base de datos de azure
        this.app.set('port', process.env.PORT || 3000);
        // mirar los tipos de peticiones que manda el cliente 
        this.app.use(morgan_1.default('dev'));
        this.app.use(cors_1.default());
        // para qe el servidor lea formato json 
        this.app.use(express_1.default.json());
        // en caso de mandar fomulario
        this.app.use(express_1.default.urlencoded({ extended: false }));
    }
    // cnfiguracion de rutas para la muestra de datos 
    routes() {
        // ruta /
        this.app.use(loginRoute_1.default);
        // ruta /home 
        this.app.use(homeRoute_1.default);
        // utilizado para los metodos de las imagenes
        this.app.use(imagenRoute_1.default);
    }
    // inicializar el servidor
    star() {
        this.app.listen(this.app.get('port'), () => {
            console.log("servidor iniciado en el puerto  ", this.app.get('port'));
        });
    }
}
const server = new Server();
server.star();
