"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class imagenController {
    ListImage(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('SELECT * FROM imagen_usuario WHERE id_usuario=' + req.body.id, function (err, result) {
                // if (err) console.error(err);
                if (result.protocol41 == true) {
                    res.json(result);
                }
                if (result == undefined || result == null) {
                    res.json({ state: false, message: "imagenes no encontradas" });
                }
            }),
                console.error("mesnsaje de error en ListImage");
            ;
        });
    }
    SelectImage(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('SELECT * FROM imagen WHERE id=' + req.body.id, function (err, result) {
                // if (err) console.error(err);
                res.json(result);
                // if (result.protocol41==true) {
                //     res.json(result);
                // }  if(result==undefined || result ==null){
                //     res.json({state:false,message:"imagen no encontrada"});
                // } 
            }),
                console.error("mesnsaje de error en ListImage");
            ;
        });
    }
    SetImage(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('INSERT INTO imagen set ?', [req.body], function (err, result) {
                // if (err) console.error(err);
                if (result.affectedRows > 0) {
                    res.json({ state: true, message: 'se ha creacdo la imagen satisfactoriamente', result: result });
                }
                else {
                    res.json({ message: "no se ha podido traer las imagenes del usuario", state: false });
                }
            }),
                console.error("mesnsaje de error en ListImage");
            ;
        });
    }
    // en este metodo se agregra los diseños que tiene cada usuario
    UserImage(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('INSERT INTO imagen_usuario set ?', [req.body], function (err, result) {
                // if (err) console.error(err);
                if (result.affectedRows > 0) {
                    res.json({ state: true, message: 'se ha creacdo la imagen satisfactoriamente' });
                }
                else {
                    res.json({ message: "no crear la imagen ", state: false });
                }
            }),
                console.error("mesnsaje de error en ListImage");
            ;
        });
    }
    GetUserImage(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('select * from imagen_usuario g inner join imagen i on i.id=g.id_imagen inner join createusers u on g.id_usuario=? and u.id=?', [req.params.id, req.params.id], function (err, result) {
                // if (err) console.error(err);
                res.json(result);
                if (result.protocol41 == true) {
                    res.json({ state: true, result: result });
                }
                else {
                    res.json({ message: "no se ha podido traer las imagenes del usuario", state: false });
                }
            }),
                console.error("mesnsaje de error en ListImage");
            ;
        });
    }
}
exports.imagen = new imagenController();
