"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class loginController {
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('SELECT * FROM createusers WHERE nombre_usuario=' + "'" + req.body.nombre_usuario + "'" + ' AND  contraseña=' + "'" + req.body.password + "'", function (err, result) {
                // if (err) console.error(err);
                if (result.protocol41 = true) {
                    res.json(result);
                }
                if (result == undefined || result == null) {
                    res.json({ state: false, message: "usuario no encontrado" });
                }
            });
        });
    }
    getusers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('SELECT * FROM createusers', function (err, result) {
                if (result.length > 0) {
                    res.json(result);
                }
                else {
                    res.json({ message: "no hay usuarios en la base de datos" });
                }
            }),
                console.error("mesnsaje de error");
            ;
        });
    }
    createUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("entro en el metodo");
            yield database_1.default.query('INSERT INTO createusers set ?', [req.body], function (err, result) {
                // if (err) console.error(err);
                if (result.protocol41 == true) {
                    res.json({ state: true, message: 'usuario creado satisfactoriamente' });
                }
                else {
                    res.json({ message: "error al crear el usuario", state: false });
                }
            }),
                console.error("mesnsaje de error");
            ;
        });
    }
    deleteUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query(' DELETE FROM createusers WHERE id=' + id, function (err, result) {
                // if (err) console.error(err);
                if (result.affectedRows > 0) {
                    res.json({ message: 'usuario eliminado satisfactoriamente ', state: true });
                }
                else {
                    res.json({ message: "error al eliminar el usuario", state: false });
                }
            }),
                console.error("mesnsaje de error");
            ;
        });
    }
    updateUSer(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query(' UPDATE createusers SET ? WHERE id= ?', [req.body, id], function (err, result) {
                // if (err) console.error(err);
                if (result.affectedRows > 0) {
                    res.json({ message: 'usuario actualizado satisfactoriamente ', state: true });
                }
                else {
                    res.json({ message: "error al actualizar el usuario", state: false });
                }
            }),
                console.error("mesnsaje de error");
            ;
        });
    }
}
exports.login = new loginController();
