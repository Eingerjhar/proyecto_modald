import express,{Application} from 'express';
import morgan from 'morgan';
import cors from 'cors';
import loginRoute from './routes/loginRoute';
import homeRoute from './routes/homeRoute';
import imagenRoute from './routes/imagenRoute';
class Server{
   public app:Application;
    constructor(){
        this.app=express();
        this.config();
        this.routes();
    }

    // configuracion del servidor 
    config():void{
        // process.env.PORT es por si necesito el puerto de a base de datos de azure
        this.app.set('port', process.env.PORT || 3000);
        // mirar los tipos de peticiones que manda el cliente 
        this.app.use(morgan('dev'));
        
        this.app.use(cors());
        // para qe el servidor lea formato json 
        this.app.use(express.json());
        // en caso de mandar fomulario
        this.app.use(express.urlencoded({extended:false}));
    }
    
    // cnfiguracion de rutas para la muestra de datos 
    routes():void{
        // ruta /
        this.app.use(loginRoute);
        // ruta /home 
        this.app.use(homeRoute);
        // utilizado para los metodos de las imagenes
        this.app.use(imagenRoute);
    }

    // inicializar el servidor
    star():void{
        this.app.listen(this.app.get('port'),()=>{
            console.log("servidor iniciado en el puerto  ",this.app.get('port'));
        });
    }
}
 const server = new Server();
 server.star();