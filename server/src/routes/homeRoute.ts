import {Router} from 'express';
import {home} from '../controllers/homeController'

class homeRoute{
    public router:Router = Router();
    constructor(){
        this.config();
    }
    config():void{
        // this.router.get('/api/home', (req,res) =>{
        //     res.send("entro en el home");
        // });

        // prueba con la base de datos

        this.router.get('/home/:id',home.home);
        // this.router.post()
    }
}
const Home = new homeRoute();
export default Home.router;