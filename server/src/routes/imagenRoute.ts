import {Router} from 'express';
import  {imagen} from '../controllers/imagenController';

class loginRoute{
    public router:Router = Router();
    constructor(){
        this.config();
    }
    config():void{
        // this.router.get('/', (req,res) =>{
        //     res.send("entro en el loginRoutes");
        // });

        // prueba con base de datos

        // pensar si se va a utilizar la lista de todos los uusarios para la prueba
        this.router.get('/listImage',imagen.ListImage);

        // post para que me valide los datos que recogo al hacer login
        this.router.get('/edit',imagen.SelectImage);

        // estos dos post son para la subida de la imagen en sus respectivas tablas
        // estos dos post deben de ser usados al mismo tiempo(mirar la base de datos para entender)
        this.router.post('/createImagen',imagen.SetImage);

        this.router.post('/create/userImage',imagen.UserImage);

        this.router.get('/get/userImage/:id',imagen.GetUserImage);

    }
}
const image = new loginRoute();
export default image.router;