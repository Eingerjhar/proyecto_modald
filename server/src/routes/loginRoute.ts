import {Router} from 'express';
import  {login} from '../controllers/loginController';
class loginRoute{
    public router:Router = Router();
    constructor(){
        this.config();
    }
    config():void{
        // this.router.get('/', (req,res) =>{
        //     res.send("entro en el loginRoutes");
        // });

        // prueba con base de datos

        // pensar si se va a utilizar la lista de todos los uusarios para la prueba
        this.router.get('/',login.getusers);

        // post para que me valide los datos que recogo al hacer login
        this.router.post('/login',login.login);

        this.router.post('/login/createUser',login.createUser);

        this.router.delete('/:id',login.deleteUser);

        // pensar en que componente va a llamar este metodo
        this.router.put('/:id',login.updateUSer);
    }
}
const Login = new loginRoute();
export default Login.router;