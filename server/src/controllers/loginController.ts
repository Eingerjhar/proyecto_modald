import { Request, Response } from 'express';
import db from '../database'

class loginController {

    public async login(req: Request, res: Response) {
        await db.query('SELECT * FROM createusers WHERE nombre_usuario=' + "'" + req.body.nombre_usuario + "'" + ' AND  contraseña=' + "'" + req.body.password + "'", function (err, result: any) {
            // if (err) console.error(err);
            if (result.protocol41=true) {
                res.json(result);
            }
            if(result==undefined || result ==null){
                res.json({state:false,message:"usuario no encontrado"});
            } 
        })
        // console.error("mesnsaje de error");
        ;
    }

    public async getusers(req: Request, res: Response) {
        await db.query('SELECT * FROM createusers', function (err, result) {
            
            if (result.length > 0) {
                res.json(result);
            } else {
                res.json({ message: "no hay usuarios en la base de datos" });
            }
        }),
        console.error("mesnsaje de error");
        ;
    }

    public async createUser(req: Request, res: Response) {
        console.log("entro en el metodo");
        await db.query('INSERT INTO createusers set ?', [req.body], function (err, result) {
            // if (err) console.error(err);
            if (result.protocol41==true) {
                res.json({ state: true, message: 'usuario creado satisfactoriamente'});
               
            } else {
                res.json({ message: "error al crear el usuario", state: false });
            }

        }),
        console.error("mesnsaje de error");
        ;
    }

    public async deleteUser(req: Request, res: Response) {
        const { id } = req.params
        await db.query(' DELETE FROM createusers WHERE id=' + id, function (err, result) {
            // if (err) console.error(err);
            if (result.affectedRows >0) {
                res.json({ message: 'usuario eliminado satisfactoriamente ', state: true });
            } else {
                res.json({ message: "error al eliminar el usuario", state: false });
            }

        }),
        console.error("mesnsaje de error");
        ;

    }
    public async updateUSer(req: Request, res: Response) {
        const { id } = req.params;
        await db.query(' UPDATE createusers SET ? WHERE id= ?', [req.body, id], function (err, result) {
            // if (err) console.error(err);
            if (result.affectedRows >0) {
                res.json({ message: 'usuario actualizado satisfactoriamente ', state: true });
            } else {
                res.json({ message: "error al actualizar el usuario", state: false });
            }

        }),
        console.error("mesnsaje de error");
        ;
    }
}
export const login = new loginController();