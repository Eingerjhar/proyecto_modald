import { Request, Response } from 'express';
import db from '../database'

class imagenController{

    public async ListImage(req:Request,res:Response){
        await db.query('SELECT * FROM imagen_usuario WHERE id_usuario='+req.body.id, function (err, result) {
            // if (err) console.error(err);
            if (result.protocol41==true) {
                res.json(result);
            }  if(result==undefined || result ==null){
                res.json({state:false,message:"imagenes no encontradas"});
            } 
        }),
        console.error("mesnsaje de error en ListImage");
        ;
    }
    public async SelectImage(req:Request,res:Response){
        await db.query('SELECT * FROM imagen WHERE id='+req.body.id, function (err, result) {
            // if (err) console.error(err);
            res.json(result);
            // if (result.protocol41==true) {
            //     res.json(result);
            // }  if(result==undefined || result ==null){
            //     res.json({state:false,message:"imagen no encontrada"});
            // } 
        }),
        console.error("mesnsaje de error en ListImage");
        ;
    }

    public async SetImage(req:Request,res:Response){
        await db.query('INSERT INTO imagen set ?',[req.body], function (err, result) {
            // if (err) console.error(err);
         
            if (result.affectedRows>0) {
                res.json({state:true,message:'se ha creacdo la imagen satisfactoriamente',result:result});
            } else {
                res.json({ message: "no se ha podido traer las imagenes del usuario", state: false });
            }
        }),
        console.error("mesnsaje de error en ListImage");
        ;
    }

    // en este metodo se agregra los diseños que tiene cada usuario
    public async UserImage(req:Request,res:Response){
        await db.query('INSERT INTO imagen_usuario set ?',[req.body], function (err, result) {
            // if (err) console.error(err);
            if (result.affectedRows>0) {
                res.json({state:true,message:'se ha creacdo la imagen satisfactoriamente'});
            } else {
                res.json({ message: "no crear la imagen ", state: false });
            }
        }),
        console.error("mesnsaje de error en ListImage");
        ;
    }

    public async GetUserImage(req:Request,res:Response){
        await db.query('select * from imagen_usuario g inner join imagen i on i.id=g.id_imagen inner join createusers u on g.id_usuario=? and u.id=?',[req.params.id,req.params.id], function (err, result) {
            // if (err) console.error(err);
                res.json(result);
            if (result.protocol41==true) {
                res.json({state:true,result:result});
            } else {
                res.json({ message: "no se ha podido traer las imagenes del usuario", state: false });
            }
        }),
        console.error("mesnsaje de error en ListImage");
        ;
    }
    
}

export const imagen = new imagenController();